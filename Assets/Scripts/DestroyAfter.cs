﻿using System;
using UnityEngine;

public class DestroyAfter : MonoBehaviour
{
    public int delay;

    public void Start()
    {
        Invoke("Destroy", delay);
    }

    public void Destroy()
    {
        Destroy(gameObject);
    }
}