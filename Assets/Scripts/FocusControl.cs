using UnityEngine;
using System.Collections;

public class FocusControl : MonoBehaviour
{
    public Camera intCam;
    public GameObject target;
    public float distance;
    private float minDist = 10;
    private float maxDist = 180;
    public bool onFocus = false;
    public GameObject focusMeter;

    void Start()
    {
        distance = 10;
    }

    void Update()
    {
        transform.position = (transform.position - target.transform.position).normalized * distance + target.transform.position;

        distance += (Input.GetAxis("Mouse ScrollWheel")*40);

        if (distance < minDist)
        {
            distance = minDist;
        }

        if (distance > maxDist)
        {
            distance = maxDist;
        }

        intCam.fieldOfView = -(distance/4.5f) + 50;
    }

    void OnTriggerStay(Collider hit)
    {
        if (hit.tag == "creature")
        {
            onFocus = true;
        }
    }

    void OnTriggerExit(Collider hit)
    {
        if (hit.tag == "creature")
        {
            onFocus = false;
        }
    }
}