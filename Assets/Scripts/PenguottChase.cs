﻿using UnityEngine;
using System.Collections;

public class PenguottChase : MonoBehaviour
{	
	public enum States
	{
		Idle,
		Chase,
		Flee
	}
	
	public States state = States.Idle;
	public Transform target;
	public float speed;
	public float rotSpeed = 2;
	private Vector3 targetDir;

    public bool snapped = false;
    public bool isFleeing = false;
	
	// Use this for initialization
	void Start ()
	{
		
		
	}//Start End
	
	// Update is called once per frame
	void Update ()
	{
		switch (state) {
		case States.Idle:
			IdleState ();
			break;
			
		case States.Chase:
			ChaseState ();
			break;
			
		case States.Flee:
			FleeState ();
			break;
			
		default:
			Debug.LogError ("ERROR: NPC should never be on Default!");
			break;
					
		}//Switch End
		//if (minionHealth == 0) {
			//transform.FindChild("Peido").GetComponent<ParticleSystem>().Play();
			
			//state = States.Die;
			//StartCoroutine ("GritaMorre");	
		//}
		
	}//Update End
	
	void IdleState ()
	{	
		targetDir = target.position - transform.position;
		if (targetDir.magnitude < 30) {
			state = States.Chase;
			return;
		}	
	}//PatrolState End
	
	
	void ChaseState ()
	{
		speed = 6.5f;
		targetDir = target.position - transform.position;
		
		if (targetDir.magnitude > 40) {
			state = States.Idle;
			return;
		}

        if (targetDir.magnitude < 40 && snapped && !isFleeing)
        {
            isFleeing = true;
            state = States.Flee;
            GameObject.Find("ExtCamera").GetComponent<CamControl>().onHurt = false;
            return;
        }

        if (targetDir.magnitude < 5 && !snapped && !isFleeing && !GameObject.Find("ExtCamera").GetComponent<CamControl>().onHurt)
        {
            GameObject.Find("ExtCamera").GetComponent<CamControl>().onHurt = true;
        }

		transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.LookRotation (targetDir), rotSpeed * Time.deltaTime);
		transform.eulerAngles = new Vector3 (0, transform.eulerAngles.y, 0);
		rigidbody.velocity = targetDir.normalized * speed;
		
	}//ChaseState End
	
	void FleeState ()
	{
        speed = 25;
        targetDir = target.position - transform.position;

        if (targetDir.magnitude > 100 && isFleeing)
        {
            Destroy(gameObject);
        }

        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(targetDir), rotSpeed * Time.deltaTime);
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
        rigidbody.velocity = -targetDir.normalized * speed;
	}

}//Class End
