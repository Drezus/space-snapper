using UnityEngine;
using System.Collections;

public class ExplosionControl : MonoBehaviour
{
	public ParticleEmitter smoke;
	public float timer;
	public float delay;
	public float force = 1000;
	public float radius = 40;
	public bool explode = true;
	
	// Use this for initialization
	void Start ()
	{
        smoke.emit = false;
	}
	
	// Update is called once per frame
	void Update ()
	{
        timer += Time.deltaTime;

        if (timer >= delay && explode)
        {
            timer = 0;
            explode = false;

            Collider[] aBricks = Physics.OverlapSphere(transform.position, radius);
            for (int i = 0; i < aBricks.Length; i++)
            {
                if (aBricks[i].rigidbody)
                {
                    aBricks[i].rigidbody.AddExplosionForce(force, transform.position, radius);
                }
            }

            smoke.emit = true;
        }

        if (timer >= delay && !explode)
        {
            if (smoke.emit)
            {
                smoke.emit = false;
                GameObject.Destroy(gameObject, 4);
            }
        }
	}
}

