using UnityEngine;

public class MusicControl : MonoBehaviour
{
    public AudioClip introMusic;
    public AudioClip loopMusic;
    public AudioSource AS;
    public GameObject fader;

    void Awake()
    {
        AS.clip = introMusic;
        AS.loop = false;
        AS.Play();
    }

    void Update()
    {
        if (AS.clip == introMusic && !AS.isPlaying)
        {
            if (Application.loadedLevel == 0)
            {
                Instantiate(fader);
                GameObject.Find("Cam").GetComponent<MenuControl>().outIntro = true;
            }
            musicLoop();
        }
    }

    public void musicLoop()
    {
        AS.clip = loopMusic;
        AS.loop = true;
        AS.Play();
    }

    public void changeMusic(AudioClip intro, AudioClip loop)
    {
        AS.Stop();
        introMusic = intro;
        loopMusic = loop;
        AS.clip = introMusic;
        AS.loop = false;
        AS.Play();
    }
}