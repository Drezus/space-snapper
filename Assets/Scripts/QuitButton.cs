﻿using System;
using UnityEngine;

public class QuitButton : MonoBehaviour
{
    public void OnMouseDown()
    {
        Application.Quit();
    }
}