﻿using System;
using UnityEngine;

public class MenuButton : MonoBehaviour
{
    public string stageName;

    public void OnMouseDown()
    {
        if(GameObject.Find("Cam").GetComponent<MenuControl>().outIntro) Application.LoadLevel(stageName);
    }
}