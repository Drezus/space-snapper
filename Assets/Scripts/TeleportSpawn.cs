﻿using System;
using UnityEngine;

public class TeleportSpawn : MonoBehaviour
{
    public GameObject car, mark, FRWheel, BLWheel, FLWheel, BRWheel, teleportParticles, startPlat;

    public void SpawnCar()
    {
        Destroy(startPlat);
        car.SetActive(true);
        mark.SetActive(true);
        FRWheel.GetComponent<Renderer>().enabled = BLWheel.GetComponent<Renderer>().enabled = FLWheel.GetComponent<Renderer>().enabled = BRWheel.GetComponent<Renderer>().enabled =  true;
        Instantiate(teleportParticles, car.transform.position, car.transform.rotation);
    }
}