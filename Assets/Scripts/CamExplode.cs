﻿using UnityEngine;
using System.Collections;

public class CamExplode : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<Light>().active = false;
        Invoke("Flash", 31.7f);
	
	}
	
	// Update is called once per frame
	void Update () {
        if (GetComponent<Light>().active)
        {
            GetComponent<Light>().intensity += GetComponent<Light>().intensity + 0.001f;
        }
	}

    void Flash()
    {
        GetComponent<Light>().active = true;
    }
}
