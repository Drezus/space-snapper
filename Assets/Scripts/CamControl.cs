using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Net.Mime;
using System.Security.Cryptography.X509Certificates;
//Adicionado para dar suporte ao sistema "Image"
//Adicionado para dar suporte ao formato da Imagem "ImageFormat.*"
using System.Collections;

public class CamControl : MonoBehaviour
{
    public Camera extCam, intCam, introMovie;
    public bool isSnapping = false;
    public bool canSnap = true;
    public bool stageEnd = false;
    public bool canControl = false;
    public bool onHurt = false;

    public ArrayList aSnaps;
    public int picCount = 20;
    public GUIText picCounterText;
    public GameObject picCounter;

    public GameObject camFlash, lowFlash, hitEffect;
    public GameObject camOverlay;

    public GameObject lastPic;

    public GameObject focus;

    public GameObject frameMeter;
    public GameObject focusMeter;

    private RaycastHit rayhit;
    private Ray ray;

    public bool framing, focusing;
    public float fielding;
    public float bestDist;
    private Texture lastSnap;

    public AudioClip resultsIntro, resultsLoop;
    public GameObject resultBG;

    public bool penguottSnapped = false;

    public float hurtTimer = 0;

    public ParticleEmitter carSmoke;
    public float damageCount = 0;


    void Start()
    {
        extCam.enabled = true;
        intCam.enabled = false;

        frameMeter.SetActive(false);
        focusMeter.SetActive(false);

        Screen.lockCursor = true;
        aSnaps = new ArrayList();
    }

    void Update()
    {
        carSmoke.maxSize = damageCount / 10;

        picCounterText.text = picCount.ToString();
        if (picCount <= 10 && picCount > 6)
        {
            picCounterText.color = Color.yellow;
        }
        if (picCount <= 5)
        {
            picCounterText.color = Color.red;
        }

        if (!stageEnd)
        {
            if (Input.GetMouseButton(1))
            {
                isSnapping = true;
                intCam.transform.eulerAngles = extCam.transform.eulerAngles;
            }
            else
            {
                isSnapping = false;
                extCam.transform.eulerAngles = intCam.transform.eulerAngles;
                frameMeter.SetActive(false);
                focusMeter.SetActive(false);
            }
        }

        extCam.enabled = !isSnapping;
        intCam.enabled = isSnapping;

        if (isSnapping)
        {
            //Framing
            if (Physics.Raycast(intCam.transform.position, intCam.transform.forward, out rayhit, 1000))
            {
                if (rayhit.collider.tag == "creature")
                {
                    frameMeter.SetActive(true);
                    framing = true;
                    if (rayhit.collider.GetComponent<PenguottChase>())
                    {
                        penguottSnapped = true;
                    }

                    if (rayhit.collider.GetComponent<Zorgeye>())
                    {
                        if (Input.GetMouseButtonDown(0) && isSnapping && canSnap)
                        {
                            rayhit.collider.GetComponent<Zorgeye>().HP--;
                        }
                    }
                }
                else
                {
                    frameMeter.SetActive(false);
                    framing = false;
                }
            }

            //Focusing
            if (focus.GetComponent<FocusControl>().onFocus && framing)
            {
                focusMeter.SetActive(true);
                focusing = true;
            }
            else if (!focus.GetComponent<FocusControl>().onFocus || !framing)
            {
                focusMeter.SetActive(false);
                focusing = false;
            }

            //Fielding
            if (framing)
            {
                var dist = Vector3.Distance(rayhit.collider.transform.position, intCam.transform.position);
                fielding = dist;

                bestDist = rayhit.collider.GetComponent<BestDist>().bestDist;
            }
            else
            {
                fielding = 0;
            }

            if (Input.GetMouseButtonDown(0) && isSnapping && canSnap)
            {
                if (picCount > 1 && !stageEnd)
                {
                    canSnap = false;
                    picCount--;
                    TakeSnap();
                }
                else if(picCount == 1 && !stageEnd)
                {
                    canSnap = false;
                    picCount--;
                    TakeSnap();

                    stageEnd = true;
                    ResultsScreen();
                }
            }
        }

        if (onHurt)
        {
            HurtDelay();
        }

        if (!onHurt && hurtTimer != 0)
        {
            hurtTimer = 0;
        }
    }

    void HurtDelay()
    {
        hurtTimer = hurtTimer + Time.deltaTime;
        if (hurtTimer > 2)
        {
            TakeDamage();
            hurtTimer = 0;
            onHurt = false;
        }
    }

    public void TakeSnap()
    {
            //Stuff de Printscreen
            camOverlay.SetActive(false);

            var aHUDPics = GameObject.FindGameObjectsWithTag("HUDPic");
            if (aHUDPics.Length > 0)
            {
                foreach (GameObject pic in aHUDPics)
                {
                    pic.guiTexture.enabled = false;
                }
            }

            Invoke("PrintScreen", 0.0001f);

            if (penguottSnapped)
            {
                var aPenguotts = GameObject.FindGameObjectsWithTag("creature");
                foreach (GameObject penguott in aPenguotts)
                {
                    var targetDir = penguott.transform.position - transform.position;
                    if (penguott.GetComponent<PenguottChase>() && targetDir.magnitude < 100 && penguott.GetComponent<PenguottChase>().state == PenguottChase.States.Chase)
                    {
                        penguott.GetComponent<PenguottChase>().snapped = true;
                    }
                }
            }
             
    }

    public void SnapFX()
    {
        createSnap();

        camOverlay.SetActive(true);

        var aHUDPics = GameObject.FindGameObjectsWithTag("HUDPic");
        if (aHUDPics.Length > 0)
        {
            foreach (GameObject pic in aHUDPics)
            {
                pic.guiTexture.enabled = true;
            }
        }

        if (picCount <= 5)
        {
            Instantiate(lowFlash);
        }
        else
        {
            Instantiate(camFlash);
        } 
    }

    public void TakeDamage()
    {
        picCount--;
        damageCount++;
        Instantiate(hitEffect);
    }

    void PrintScreen()
    {
        int startX = 0;
        int startY = 0;
        int width = Screen.width;
        int height = Screen.height;
        Texture2D tex = new Texture2D(width, height, TextureFormat.RGB24, false);
        tex.ReadPixels(new Rect(startX, startY, width, height), 0, 0);
        tex.Apply();

		//var bytes = tex.EncodeToPNG();
        //Destroy(tex);

        if (GameObject.FindWithTag("HUDPic"))
        {
            Destroy(GameObject.FindWithTag("HUDPic"));
        }
        var pic = Instantiate(lastPic) as GameObject;
        pic.guiTexture.texture = tex;
        lastSnap = tex;

        SnapFX();

        //File.WriteAllBytes(Application.dataPath + "/Fotos/foto" + rodada.ToString() + ".png", bytes);
    }

    void createSnap()
    {
        var snap = new Photo();
        snap.framing = framing;
        snap.focusing = focusing;
        snap.fielding = (1/fielding);
        snap.imgSnap = lastSnap;

        if (framing)
        {
            snap.rating = 1;
            if (focusing)
            {
                snap.rating = 2;
                if (snap.fielding > 0.02)
                {
                    snap.rating = 3;
                }
            }
        }

        if (snap.rating == 1)
        {
            snap.points = 100;
        }
        else if (snap.rating == 2)
        {
            snap.points = 200;
        }
        else if (snap.rating == 3)
        {
            snap.points = 300 + ((int)snap.fielding * 10);
        }

        print(snap.rating + " |||| " + snap.fielding);

        aSnaps.Add(snap);
    }

    //NECESSITA DE SYSTEM.DRAWING
    //void ConvertAndSave()
    //{
    //    int rodada = GameObject.Find("DataHolder").GetComponent<DataGlobal>().rodadaGlobal;
    //    Image png = Image.FromFile(Application.dataPath + "/Fotos/foto" + rodada.ToString() + ".png");
    //    png.Save(Application.dataPath + "/Fotos/foto" + rodada.ToString() + ".jpg", ImageFormat.Jpeg);
    //    png.Dispose();
    //    File.Delete(Application.dataPath + "/Fotos/foto" + rodada.ToString() + ".png");

    //    Application.LoadLevel("10-receitas");
    //}

    public void ResultsScreen()
    {
        intCam.GetComponent<MusicControl>().changeMusic(resultsIntro, resultsLoop);
        intCam.GetComponent<MouseLook>().enabled = false;
        Instantiate(resultBG);
        Invoke("RestoreMouse", 5);
    }

    void RestoreMouse()
    {
        Screen.lockCursor = false;
    }
}