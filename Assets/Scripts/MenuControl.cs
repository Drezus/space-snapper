using UnityEngine;

public class MenuControl : MonoBehaviour
{
    public GameObject fader;
    public AudioSource AS;
    public GameObject movieCam;
    public GameObject mark;
    public GameObject photos1, photos2;

    public bool outIntro = false;

    void Start()
    {
        mark.SetActive(false);
        photos1.SetActive(false);
        photos2.SetActive(false);
        Invoke("Rise", 16);
    }

    void Update()
    {


        if (!outIntro)
        {
            if(Input.GetKeyDown(KeyCode.Return) ||Input.GetKeyDown(KeyCode.KeypadEnter))
            {
                Instantiate(fader);

                AS.GetComponent<MusicControl>().musicLoop();
                outIntro = true;
            }
        }
    }

    void Rise()
    {
        if (!outIntro)
        {
            mark.SetActive(true);
            photos1.SetActive(true);
            photos2.SetActive(true);
        }
    }
}