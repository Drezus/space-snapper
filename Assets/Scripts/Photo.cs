﻿using System;
using UnityEngine;

public class Photo
{
    public bool framing;
    public bool focusing;
    public float fielding;
    public Texture imgSnap;

    public int points;
    public int rating = 0;
}