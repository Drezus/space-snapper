﻿using System;
using UnityEngine;

public class Zorgeye : MonoBehaviour
{
    public GameObject snapCar;
    public Rigidbody laserPrefab;
    public GameObject explosionPrefab;
    private float shootCounter = 0;
    public int HP = 3;
    public bool isDead = false;

    public void Update()
    {
        transform.LookAt(snapCar.transform.position);

        shootCounter = shootCounter + Time.deltaTime;

        var targetDir = snapCar.transform.position - transform.position;

        if (shootCounter > 2.5f && !isDead)
        {
            if (targetDir.magnitude <= 30)
            {
                ShootLaser();
            }

            shootCounter = 0;
        }

        if (HP <= 0 && !isDead)
        {
            isDead = true;
            Die();
        }
    }

    public void OnTriggerEnter(Collider hit)
    {
        if (hit.tag == "pit")
        {
            Destroy(gameObject);
        }
    }

    public void ShootLaser()
    {
        var ls = Instantiate(laserPrefab, transform.position, transform.rotation) as Rigidbody;
        ls.AddForce(transform.forward * 500);
    }

    public void Die()
    {
        animation.Play("zorgeye-death");
        Invoke("Explode", 1);
    }

    public void Explode()
    {
        Instantiate(explosionPrefab, transform.position, transform.rotation);
        Destroy(gameObject);
    }
}