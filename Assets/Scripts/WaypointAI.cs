﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class WaypointAI : MonoBehaviour
{
    public Transform[] waypoints;
    public int currentWayPoint;
    public float transVel;
    public float rotVel;

    public void Start()
    {
        currentWayPoint = 0;
    }

    public void Update()
    {
        FollowWayPoints();
    }

    private void FollowWayPoints()
    {
        Vector3 dir = waypoints[currentWayPoint].position - transform.position;

        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(dir), Time.deltaTime * rotVel);
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);

        if (dir.magnitude <= 1)
        {
            currentWayPoint++;

            if (currentWayPoint >= waypoints.Length)
            {
                currentWayPoint = 0;
            }
        }
        else
        {
            //rigidbody.velocity = dir.normalized * transVel;
        }
    }
}