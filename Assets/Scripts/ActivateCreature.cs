﻿using System;
using UnityEngine;

public class ActivateCreature : MonoBehaviour
{
    public GameObject targetCreature;

    public void Start()
    {
        targetCreature.SetActive(false);
    }
}