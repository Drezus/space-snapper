﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class MoveAICar : MonoBehaviour
{
    // These variables allow the script to power the wheels of the car.
    public WheelCollider frontLeftWheel;
    public WheelCollider frontRightWheel;

    // These variables are for the gears, the array is the list of ratios. The script
    // uses the defined gear ratios to determine how much torque to apply to the wheels.
    public float[] gearRatio;
    public int currentGear = 0;

    // Here's all the variables for the AI, the waypoints are determined in the "GetWaypoints" function.
    // the waypoint container is used to search for all the waypoints in the scene, and the current
    // waypoint is used to determine which waypoint in the array the car is aiming for.
    public GameObject waypointContainer;
    private List<Transform> waypoints;
    private int currentWaypoint = 0;

    // These variables are just for applying torque to the wheels and shifting gears.
    // using the defined Max and Min Engine RPM, the script can determine what gear the
    // car needs to be in.
    public float engineTorque = 600F;
    public float maxEngineRPM = 3000F;
    public float minEngineRPM = 1000F;
    private float engineRPM;

    // input steer and input torque are the values substituted out for the player input. The 
    // "NavigateTowardsWaypoint" function determines values to use for these variables to move the car
    // in the desired direction.
    private float inputSteer  = 0.0F;
    private float inputTorque = 0.0F;

    public static bool canControl;

    public void Start()
    {
        canControl = true;

        // This makes the car more stable
        rigidbody.centerOfMass = new Vector3(rigidbody.centerOfMass.x,
                                             -1.5F,
                                             rigidbody.centerOfMass.z);

        // Create the array with the gear ratios
        gearRatio = new float[1];
        gearRatio[0] = 7F;
       // gearRatio[1] = 2.71F;
       // gearRatio[2] = 1.88F;
       // gearRatio[3] = 1.41F;
       // gearRatio[4] = 1.13F;
       // gearRatio[5] = 0.93F;

        // Call the function to determine the array of waypoints. This sets up the array of points by finding
        // transform components inside of a source container.
        FindWayPoints();
    }

    public void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            Time.timeScale = 5;
        }
        else
        {
            Time.timeScale = 1;
        }

        if (canControl)
        {
            // This is to limith the maximum speed of the car
            rigidbody.drag = rigidbody.velocity.magnitude / 250F;

            // Call the funtion to determine the desired input values for the car. This essentially steers and
            // applies gas to the engine.
            NavigateTowardsWaypoint();

            // Compute the engine RPM based on the average RPM of the two wheels, then call the shift gear function
            engineRPM = (frontLeftWheel.rpm + frontRightWheel.rpm) / 2 * gearRatio[currentGear];
            ShiftGears();

            // Apply the values to the wheels. The torque applied is divided by the current gear, and
            // multiplied by the user input variable.
            frontLeftWheel.motorTorque = (engineTorque / gearRatio[currentGear]) * inputTorque;
            frontRightWheel.motorTorque = (engineTorque / gearRatio[currentGear]) * inputTorque;

            // The steer angle is an arbitrary value multiplied by the user input.
            frontLeftWheel.steerAngle = 10 * inputSteer;
            frontRightWheel.steerAngle = 10 * inputSteer;
        }
    }

    private void ShiftGears()
    {
        // This funciton loops through all the gears, checking which will make
        // the engine RPM fall within the desired range. The gear is then set to this appropriate value.
        if (engineRPM >= maxEngineRPM)
        {
            int appropriateGear = currentGear;

            for (int i = 0; i < gearRatio.Length; i++)
            {
                if (frontLeftWheel.rpm * gearRatio[i] < maxEngineRPM)
                {
                    appropriateGear = i;
                    break;
                }
            }

            currentGear = appropriateGear;
        }

        if (engineRPM <= minEngineRPM)
        {
            int appropriateGear = currentGear;

            for (int j = gearRatio.Length - 1; j >= 0; j--)
            {
                if (frontLeftWheel.rpm * gearRatio[j] > minEngineRPM)
                {
                    appropriateGear = j;
                    break;
                }
            }

            currentGear = appropriateGear;
        }

    }

    private void FindWayPoints()
    {
        // Now, this function basically takes the container object for the waypoints, 
        //   then finds all of the transforms in it, once it has the transforms, 
        //   it checks to make sure it's not the container, and adds them to the array of waypoints.
        Transform[] potentialWayPoints = waypointContainer.GetComponentsInChildren<Transform>();
        waypoints = new List<Transform>();

        for (int i = 1; i < potentialWayPoints.Length; i++)
        {
            waypoints.Add(potentialWayPoints[i]);
        }
    }


    private void NavigateTowardsWaypoint()
    {
        // now we just find the relative position of the waypoint from the car transform,
        // that way we can determine how far to the left and right the waypoint is.
        Vector3 relativeWayPointPos =
            transform.InverseTransformPoint(new Vector3(waypoints[currentWaypoint].position.x,
                                                        transform.position.y,
                                                        waypoints[currentWaypoint].position.z));

        // by dividing the horizontal position by the magnitude, 
        //   we get a decimal percentage of the turn angle that we can use to drive the wheels
        inputSteer = relativeWayPointPos.x / relativeWayPointPos.magnitude;

        if (Mathf.Abs(inputSteer) < 0.5F)
        {
            inputTorque = (relativeWayPointPos.z / relativeWayPointPos.magnitude) - Mathf.Abs(inputSteer);
        }
        else
        {
            inputTorque = 0.0F;
        }

        // this just checks if the car's position is near enough to a waypoint to count as passing it, 
        //   if it is, then change the target waypoint to the next in the list.
        if (relativeWayPointPos.magnitude < 20)
        {
            currentWaypoint++;
            if (currentWaypoint >= waypoints.Count)
            {
                currentWaypoint = 0;
            }

        }
    }

    public void OnTriggerEnter(Collider hit)
    {
        if (hit.tag == "changeGear")
        {
            gearRatio[0] = hit.GetComponent<ChangeGear>().desiredValue;
        }

        if (hit.tag == "creatureSensor")
        {
            hit.GetComponent<ActivateCreature>().targetCreature.SetActive(true);
        }

        if (hit.tag == "laser")
        {
            Destroy(hit.gameObject);
            GameObject.Find("ExtCamera").GetComponent<CamControl>().TakeDamage();
        }
    }
}
