﻿using System;
using UnityEngine;

public class TimedDestroy : MonoBehaviour
{
    public float Delay;

    public void Start()
    {
        Destroy(gameObject, Delay);
    }
}