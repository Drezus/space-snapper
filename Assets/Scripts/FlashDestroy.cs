﻿using System;
using UnityEngine;

public class FlashDestroy : MonoBehaviour
{
    public void Destroy()
    {
        if (GameObject.Find("ExtCamera").GetComponent<CamControl>().picCount > 0)
        {
            GameObject.Find("ExtCamera").GetComponent<CamControl>().canSnap = true;
        }
        Destroy(gameObject);
    }
}