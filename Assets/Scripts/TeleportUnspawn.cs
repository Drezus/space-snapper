﻿using System;
using UnityEngine;

public class TeleportUnspawn : MonoBehaviour
{
    public GameObject snapCar, FRWheel, BLWheel, FLWheel, BRWheel, teleportParticles;

    public void SpawnCar()
    {
        Instantiate(teleportParticles, snapCar.transform.position, snapCar.transform.rotation);
        snapCar.GetComponent<Renderer>().enabled = FRWheel.GetComponent<Renderer>().enabled = BLWheel.GetComponent<Renderer>().enabled = FLWheel.GetComponent<Renderer>().enabled = BRWheel.GetComponent<Renderer>().enabled = false;
    }
}