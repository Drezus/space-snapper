﻿using System;
using UnityEngine;

public class FadeOut : MonoBehaviour
{
    public GameObject fadeIn;
    private AudioSource AS;
    public GameObject logo;

    public void Awake()
    {
        AS = GameObject.Find("Cam").GetComponent<AudioSource>();
    }

    public void EndFade()
    {
        Instantiate(fadeIn);
        Instantiate(logo);
        GameObject.Find("Cam").GetComponent<MenuControl>().movieCam.SetActive(false);
        Destroy(gameObject);
    }
}